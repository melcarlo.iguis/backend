const mongoose = require('mongoose')
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required : [true, 'Total Amount is is required']
	},

	purchasedOn: {
		type: Date,
		default : new Date()
	},

	user: [
	{
		userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' , required : true},

		userEmail : {
			type: String,
			required : [true, 'Email is required']
		}
	}],

	product: [
	{
		productId : { type: mongoose.Schema.Types.ObjectId, ref: 'Product' , required: true},

		price : {
			type: Number,
			required : [true, 'price name is required']
		},

		quantity: {
			type: Number ,
			required: [true, 'Quantity is required']
		}
	}]
	
	
})

module.exports = mongoose.model('Order', orderSchema)