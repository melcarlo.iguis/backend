const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({

	email : {
		type: String,
		required : [true, 'Email is required']
	},

	password : {
		type: String,
		required : [true, 'Password is required']
	},
	
	isAdmin : {
		type: Boolean,
		default : false
	},

	order: [
	{
		orderId: {
			type: String,
			required : [true, 'order Id is required']
		},

		totalAmount: {
			type: Number,
			required : [true, 'Total Amount is is required']
		},
		product: [
		{
			productId : {
				type: String,
				required : [true, 'product Id is required']
			},

			productName : {
				type: String,
				required : [true, 'Product name is required']
			}
		}]
	}]

	
})

module.exports = mongoose.model('User', userSchema)