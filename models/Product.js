const mongoose = require('mongoose')
const productSchema = new mongoose.Schema({

	name : {
		type: String,
		required : [true, 'Name is required']
	},

	description : {
		type: String,
		required : [true, 'Description is required']
	},

	productPicture : { img : {type: String}
	},

	category : {
		type: String,
		required : [true, 'Category is required']
	},


	price : {
		type: Number,
		required : [true, 'Price is required']
	},

	numberOfStock: {
		type: Number, 
		required : [true , 'Number of Stock is required']	
	},
	
	isActive : {
		type: Boolean,
		default : true
	},

	createdOn : {
		type: Date,
		default : new Date()
	}

})

module.exports = mongoose.model('Product', productSchema)