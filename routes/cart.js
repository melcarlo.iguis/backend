const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cart');
const auth = require('../auth');

// - Add item to cart
router.post('/addToCart', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {

		product: req.body.cartItems.product,
		quantity: req.body.cartItems.quantity,
		price : req.body.cartItems.price,
        cartItems : req.body.cartItems,
		userId: userData.id,
		productId: req.body.productId
	}
	
	if(!userData.isAdmin){
		cartController.addToCart(data).then(resultFromController =>
		res.send(resultFromController));
	}else{
		res.send("you're an admin, you're not allowed to add item to cart")
	}
})

// display all cart (Admin only)
router.get('/getAllCart', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		cartController.allCart(data).then(resultFromController =>
		res.send(resultFromController));
	}else{
		res.send("you're not admin, you're not allowed to check all the cart")
	}
})


module.exports = router;