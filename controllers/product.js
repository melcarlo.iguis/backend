const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const shortid = require('shortid')

// -  Create Product (Admin only) - controller
module.exports.createProduct = (req) => {

	const {
		name ,description , category ,price  , numberOfStock	
	} = req.body;

	const productPicture = {img: req.file.filename};
	
	let newProduct = new Product({

		name: name,
		description,
		price,
		productPicture,
		category,
		numberOfStock

	})

	// Condition for checking product duplicate
	return Product.find({name: req.body.name}).then(result =>{

		if(result.length > 0 ){
			console.log("Product already exist");
			return("This product name already exist, failed to add new product")
		}else{
			return newProduct.save().then((addProduct,error)=> {
				if(error){
					return res.status(400).json({error});
				}else if(addProduct){
					console.log("Product has been added")
					return ({addProduct})

				}
			})
		}
		
	})
}

// - Retrieve all active products controller
module.exports.getAllActice = () => {
	return Product.find({isActive: true}).then(result =>{
		return result
	})
}

// - Retrieve single product controller
module.exports.getSingleProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result =>{
		return result
	})

}

// -  Update Product information (Admin only) - controller
module.exports.updateProduct = (reqParams, req) =>{

	const {
		name ,description , category ,price  , numberOfStock	
	} = req.body;

	const productPicture = {img: req.file.filename};
	
	let updatedProduct = {

		name: name,
		description,
		category,
		numberOfStock,
		price,
		productPicture

	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((update,error) =>{
		if(error){
			return false
		}else{
			console.log('product updated successfully')
			return true

		}
	})
}

// // - Archive Product (Admin only) - controller
module.exports.archiveProduct = (reqParams) =>{

	let updatedProduct = {
		isActive : false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((update,error) =>{
		if(error){
			return false
		}else{
			console.log('product archived successfully,')
			return true
		}
	})
}