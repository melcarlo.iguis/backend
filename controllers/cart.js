const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const Cart = require('../models/Cart');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Add cart routes here 
module.exports.addToCart = (data) => {

    return Cart.findOne({ user: data.userId}).then(result =>{
        if(result){
    
           console.log("Cart already exist");
           const item = result.cartItems.find(c => c.product == data.cartItems.product);
             //item.product == data.cartItems.product
           if(item){
            console.log("product already added to cart. proceed to add quantity")
            Cart.findOneAndUpdate({user: data.userId, "cartItems.product" : data.cartItems.product}, {
                "$set" : {
                    "cartItems.$" : {
                        ...data.cartItems,
                        quantity: item.quantity + data.cartItems.quantity,
                        price: data.price * (item.quantity + data.cartItems.quantity) 
                    }
                }
            })
            .exec((error, _cart)=> {
                if(error) return ({error});
                else if(_cart){
                    console.log("succesfully added quantity")
                    return({_cart});
                } 
            })
           }else{
            console.log("Added new item ");
            Cart.findOneAndUpdate({user: data.userId}, {
                "$push" : {
                    "cartItems" : {
                        product: data.product,
                        quantity: data.quantity,
                        price: data.price * data.quantity
                    }
                }
            })
            .exec((error, _cart)=> {
                if(error) return ({error});
                else if(_cart) return({_cart});
            })
           }
            
        }else{
    
            let cartDetails = new Cart({
    
                user: data.userId,
                cartItems: {
                    product: data.product,
                    quantity: data.quantity,
                    price: data.price * data.quantity
                }
        
            });
    
            return cartDetails.save().then((save,error)=> {
                if(error){
                    console.log("Failed to add item to cart")
                    return false
                }else if(save){
                    console.log("Item has been added to cart")
                    return ({save})
        
                }
            })
        }
        
    })
   
}


// get all cart 
module.exports.allCart = () => {
	return Cart.find({}).then(result =>{
		return result
	})
}

    

